﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using MyBankApp.Core.Constants;
using MyBankApp.Core.Services;
using MyBankApp.Core.Services.Contracts;
using MyBankApp.Core.ViewModels;
using MyBankApp.Infrastructure.Entities;
using MyBankApp.Infrastructure.Repositories;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MyBankApp.Test.PurchaseServiceTests
{
    public class PurchaseServiceTests
    {
        private ServiceProvider serviceProvider;
        private InMemoryDbContext dbContext;

        [SetUp]
        public async Task Setup()
        {
            dbContext = new InMemoryDbContext();
            var serviceCollection = new ServiceCollection();


            serviceProvider = serviceCollection
                .AddSingleton(sp => dbContext.CreateContext())
                .AddSingleton<IApplicatioDbRepository, ApplicatioDbRepository>()
                .AddSingleton<IPurchaseService, PurchaseService>()
                .AddSingleton<IHttpContextAccessor, HttpContextAccessor>()
                .BuildServiceProvider();

            var repo = serviceProvider.GetService<IApplicatioDbRepository>();
            await SeedDbAsync(repo);
        }

        // BuyCurrencyView();

        [Test]
        public async Task BuyCurrencyViewShouldReturnBuyCurrencyViewModel()
        {
            string currencyName = "BGN";
            decimal currencyPrice = 1;

            var service = serviceProvider.GetService<IPurchaseService>();

            var model = service.BuyCurrencyView(currencyName, currencyPrice);

            Assert.IsInstanceOf(typeof(BuyCurrencyViewModel), model);

            TearDown();
        }

        [Test]
        public async Task SellCurrencyViewReturnsCorrectResult()
        {
            string currencyName = "BGN";
            decimal currencyPrice = 1;
            decimal amount = 1;

            var currencymodel = new CurrencyViewModel()
            {
                Base = "USD",
                Rates = new List<Rate>()
                {
                    new Rate()
                    {
                        Name = currencyName,
                        Percent = currencyPrice,
                        Amount = amount
                    }

                }
            };

            var service = serviceProvider.GetService<IPurchaseService>();

            var model = await service.SellCurrencyView(currencyName, currencyPrice, currencymodel);

            Assert.IsInstanceOf(typeof(SellCurrencyViewModel), model);

            TearDown();
        }

        // SellCurrency();

        [Test]
        public void CellCurrencyThrowExceptionWhenUserIsNull()
        {
            var model = new SellCurrencyInputModel()
            {
                Name = "BGN",
                AmountInUsd = 0.5M,
                SellAmount = 1
            };

            string userName = "fake";

            var service = serviceProvider.GetService<IPurchaseService>();

            var res = Assert.CatchAsync<ArgumentNullException>(async () => await service.SellCurrency(model, userName));

            Assert.AreEqual(MessageConstant.UserNullMessage, res.ParamName);

            TearDown();
        }

        [Test]
        public void CellCurrencyThrowExceptionWhenCurrencyIsNotFound()
        {
            var model = new SellCurrencyInputModel()
            {
                Name = "BGN",
                AmountInUsd = 0.5M,
                SellAmount = 1
            };

            string userName = "asd@asd.asd";

            var service = serviceProvider.GetService<IPurchaseService>();

            var res = Assert.CatchAsync<ArgumentNullException>(async () => await service.SellCurrency(model, userName));

            Assert.AreEqual(MessageConstant.CurrencyNullMessage, res.ParamName);

            TearDown();
        }

        [Test]
        public void CellCurrencyThrowExceptionWhenUserAccountIsNull()
        {
            var model = new SellCurrencyInputModel()
            {
                Name = "BGN",
                AmountInUsd = 0.5M,
                SellAmount = 1
            };

            string userName = "dsa@dsa.dsa";

            var service = serviceProvider.GetService<IPurchaseService>();

            var res = Assert.CatchAsync<ArgumentNullException>(async () => await service.SellCurrency(model, userName));

            Assert.AreEqual(MessageConstant.AccountNullMessage, res.ParamName);

            TearDown();
        }

        public void TearDown()
        {
            dbContext.Dispose();
        }

        private async Task SeedDbAsync(IApplicatioDbRepository repo)
        {
            var user = new ApplicationUser()
            {
                Id = "asd",
                UserName = "asd@asd.asd",
                FirstName = "Pesho",
                LastName = "Peshev",
                IsDeleted = false,
                Accounts = new List<Account>()
                {
                    new Account()
                    {
                        Id = new Guid("41d3892f-9df5-4787-ada7-38704e5fff41"),
                        Balance = 12,
                        Date = DateTime.Now,
                        ApplicationUserId = "12345",
                        AccountNumber = "1111111111111111111111",
                        NickName = "Pesho",
                    }
                },
                

            };

            var secondUser = new ApplicationUser()
            {
                Id = "dsa",
                UserName = "dsa@dsa.dsa",
                FirstName = "Gosho",
                LastName = "Geshev",
                IsDeleted = false,
                Accounts = new List<Account>()
            };

            //var rate = new Rate()
            //{
            //    Id = new Guid("48fa248d-e4e9-40ed-b155-75bf61ab4e4a"),
            //    Name = "BGN",
            //    Percent = 1.8M
            //};

            //var userRate = new UserRate()
            //{
            //    User = user,
            //    Rate = rate
            //};

            //await repo.AddAsync(rate);
            // await repo.AddAsync(userRate);

            await repo.AddAsync(user);
            await repo.AddAsync(secondUser);
            await repo.SaveChangesAsync();
        }
    }
}
