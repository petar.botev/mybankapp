﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Moq;
using Moq.Protected;
using MyBankApp.Core.Services.Contracts;
using MyBankApp.Infrastructure.Entities;
using MyBankApp.Infrastructure.Repositories;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace MyBankApp.Test.InitialPageServiceTests
{
    public class InitialPageServiceTests
    {
        private ServiceProvider serviceProvider;
        private InMemoryDbContext dbContext;

        [SetUp]
        public async Task Setup()
        {
            dbContext = new InMemoryDbContext();
            var serviceCollection = new ServiceCollection();

            serviceProvider = serviceCollection
                .AddSingleton(sp => dbContext.CreateContext())
                .AddSingleton<IApplicatioDbRepository, ApplicatioDbRepository>()
                .AddSingleton<IHttpContextAccessor, HttpContextAccessor>()
                //.AddSingleton<IInitialPageService, InitialPageService>()
                .BuildServiceProvider();

            var repo = serviceProvider.GetService<IApplicatioDbRepository>();
            await SeedDbAsync(repo);
        }

        private Mock<IHttpClientFactory> MockHttpClientFactory()
        {
            var handlerMock = new Mock<HttpMessageHandler>(MockBehavior.Strict);

            HttpResponseMessage result = new HttpResponseMessage();

            handlerMock
                .Protected()
                .Setup<Task<HttpResponseMessage>>(
                    "SendAsync",
                    ItExpr.IsAny<HttpRequestMessage>(),
                    ItExpr.IsAny<CancellationToken>()
                )
                .ReturnsAsync(result)
                .Verifiable();

            var httpClient = new HttpClient(handlerMock.Object)
            {
                BaseAddress = new Uri("https://www.code4it.dev/")
            };

            var mockHttpClientFactory = new Mock<IHttpClientFactory>();

            mockHttpClientFactory.Setup(_ => _.CreateClient("ext_service")).Returns(httpClient);

            return mockHttpClientFactory;
        }

        //[Test]
        //public async Task GetCurrencyDataShouldReturnCurrencyViewModel()
        //{
        //    var mock = MockHttpClientFactory();

        //    var initialPage = new Mock<IInitialPageService>();

            

        //    //var model = await service.GetCurrencyData();


        //   // Assert.IsInstanceOf(typeof(CurrencyViewModel), model);

        //    TearDown();
        //}

        public void TearDown()
        {
            dbContext.Dispose();
        }

        private async Task SeedDbAsync(IApplicatioDbRepository repo)
        {
            var user = new ApplicationUser()
            {
                Id = "asd",
                UserName = "asd@asd.asd",
                FirstName = "Pesho",
                LastName = "Peshev",
                IsDeleted = false,
                Accounts = new List<Account>()
                {
                    new Account()
                    {
                        Id = new Guid("41d3892f-9df5-4787-ada7-38704e5fff41"),
                        Balance = 12,
                        Date = DateTime.Now,
                        ApplicationUserId = "12345",
                        AccountNumber = "1111111111111111111111",
                        NickName = "Pesho",
                    }
                }
            };

            var secondUser = new ApplicationUser()
            {
                Id = "dsa",
                UserName = "dsa@dsa.dsa",
                FirstName = "Gosho",
                LastName = "Geshev",
                IsDeleted = false,
                Accounts = new List<Account>()
            };

            await repo.AddAsync(user);
            await repo.AddAsync(secondUser);
            await repo.SaveChangesAsync();
        }
    }
}
