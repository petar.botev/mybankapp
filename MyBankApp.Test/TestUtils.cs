﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using MyBankApp.Infrastructure.Data;
using MyBankApp.Infrastructure.Repositories;

namespace PaySharp.Tests
{
    public class TestUtils
    {
        public static DbContextOptions<ApplicationDbContext> GetOptions(string databaseName)
        {
            var provider = new ServiceCollection()
            .AddEntityFrameworkInMemoryDatabase()
            .AddSingleton<IApplicatioDbRepository, ApplicatioDbRepository>()
            .BuildServiceProvider();

            return new DbContextOptionsBuilder<ApplicationDbContext>()
            .UseInMemoryDatabase(databaseName)
            .UseInternalServiceProvider(provider)
            .Options;
        }
    }
}
