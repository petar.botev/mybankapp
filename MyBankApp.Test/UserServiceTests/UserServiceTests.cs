﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using MyBankApp.Core.Constants;
using MyBankApp.Core.Services;
using MyBankApp.Core.Services.Contracts;
using MyBankApp.Core.ViewModels;
using MyBankApp.Infrastructure.Entities;
using MyBankApp.Infrastructure.Repositories;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyBankApp.Test.UserServiceTests
{
    public class UserServiceTests
    {
        private ServiceProvider serviceProvider;
        private InMemoryDbContext dbContext;

        [SetUp]
        public async Task Setup()
        {
            dbContext = new InMemoryDbContext();
            var serviceCollection = new ServiceCollection();

            serviceProvider = serviceCollection
                .AddSingleton(sp => dbContext.CreateContext())
                .AddSingleton<IApplicatioDbRepository, ApplicatioDbRepository>()
                .AddSingleton<IUserService, UserService>()
                .AddSingleton<ICommonService, CommonService>()
                .AddSingleton<IHttpContextAccessor, HttpContextAccessor>()
                .BuildServiceProvider();

            var repo = serviceProvider.GetService<IApplicatioDbRepository>();
            await SeedDbAsync(repo);

        }


        // GetAll();

        [Test]
        public void GetAllReturnsApplicationUserViewModelCollection()
        {
            var service = serviceProvider.GetService<IUserService>();

            var result = service.GetAll();

            Assert.IsInstanceOf(typeof(List<ApplicationUserViewModel>), result);

            TearDown();
        }

        // Delete();

        [Test]
        public void DeleteShouldThrowExceptionWhenUserIsNotFound()
        {
            var service = serviceProvider.GetService<IUserService>();

            var id = "a";

            var result = Assert.CatchAsync<ArgumentNullException>(async () => await service.Delete(id), MessageConstant.UserNullMessage);
            Assert.AreEqual(MessageConstant.UserNullMessage, result.ParamName);

            TearDown();
        }

        [Test]
        public async Task DeleteShouldSetIsDeletedToTrue()
        {
            var service = serviceProvider.GetService<IUserService>();

            var id = "asd";

            await service.Delete(id);

            var repo = serviceProvider.GetService<IApplicatioDbRepository>();

            var user = repo.All<ApplicationUser>().FirstOrDefault(x => x.Id == id);

            Assert.AreEqual(user.IsDeleted, true);

            TearDown();
        }

        //GetUser();

        //[Test]
        //public async Task GetUserReturnsEditUserViewModel()
        //{
        //    var service = serviceProvider.GetService<IUserService>();
        //    var repo = serviceProvider.GetService<IApplicatioDbRepository>();

        //    var id = "asd";
        //    var user = repo.All<ApplicationUser>()
        //        .FirstOrDefault(x => x.Id == id);
        //    var roles = new List<string>() { "Random" };

        //    var result = service.GetUser(id, user, roles);

        //    Assert.IsInstanceOf(typeof(EditUserViewModel), result);

        //    TearDown();
        //}

        // Edit();

        [Test]
        public async Task EditUser()
        {
            var service = serviceProvider.GetService<IUserService>();
            var repo = serviceProvider.GetService<IApplicatioDbRepository>();

            var model = new EditUserViewModel()
            {
                Id = "asd",
                FirstName = "a",
                LastName = "b",
                Email = "a@b.c",
                IsDeleted = true
            };

            await service.Edit(model);

            var newUser = new ApplicationUser()
            {
                FirstName = model.FirstName,
                LastName = model.LastName,
                Email = model.Email,
                IsDeleted = model.IsDeleted
            };

            await repo.AddAsync(newUser);
            await repo.SaveChangesAsync();

            var user = repo.All<ApplicationUser>()
                .FirstOrDefault(x => x.Email == model.Email);

            Assert.IsNotNull(user);
            Assert.AreEqual("a", user.FirstName);
            Assert.AreEqual("b", user.LastName);
            Assert.AreEqual("a@b.c", user.Email);
            Assert.IsTrue(user.IsDeleted);

            TearDown();
        }

        [Test]
        public void EditShouldThrowExceptionWhenUserIsNull()
        {
            var service = serviceProvider.GetService<IUserService>();

            var model = new EditUserViewModel()
            {
                Id = "aa",
                FirstName = "a",
                LastName = "b",
                Email = "a@b.c",
                IsDeleted = true
            };

            var result = Assert.CatchAsync<ArgumentNullException>(async () => await service.Edit(model), MessageConstant.UserNullMessage);
            Assert.AreEqual(MessageConstant.UserNullMessage, result.ParamName);

            TearDown();
        }


        public void TearDown()
        {
            dbContext.Dispose();
        }


        private async Task SeedDbAsync(IApplicatioDbRepository repo)
        {
            var user = new ApplicationUser()
            {
                Id = "asd",
                UserName = "asd@asd.asd",
                FirstName = "Pesho",
                LastName = "Peshev",
                IsDeleted = false,
                Accounts = new List<Account>()
                {
                    new Account()
                    {
                        Id = new Guid("41d3892f-9df5-4787-ada7-38704e5fff41"),
                        Balance = 12,
                        Date = DateTime.Now,
                        ApplicationUserId = "12345",
                        AccountNumber = "1111111111111111111111",
                        NickName = "Pesho",
                    }
                },
                Transactions = new List<Transaction>()
                {
                    new Transaction()
                    {
                        Sum = 10,
                        Description = "aaa",
                        Date = DateTime.Now,
                        SenderAccountID = new Guid("41d3892f-9df5-4787-ada7-38704e5fff41"),
                        ReceiverAccountID = new Guid("4e58fbe1-7ad7-4188-aa82-5960ede74979"),
                        UserId = "asd"
                    }
                }
            };

            var secondUser = new ApplicationUser()
            {
                Id = "dsa",
                UserName = "dsa@dsa.dsa",
                FirstName = "Gosho",
                LastName = "Geshev",
                IsDeleted = false,
                Accounts = new List<Account>()
            };

            var sender = new ApplicationUser()
            {
                Id = "aaa",
                UserName = "aaa@aaa.aaa",
                FirstName = "Tosho",
                LastName = "Toshev",
                IsDeleted = false,
                Accounts = new List<Account>()
                {
                    new Account()
                    {
                        Id = new Guid("ba3fa946-8749-4c66-8266-05b8a14e1ae0"),
                        Balance = 10,
                        Date = DateTime.Now,
                        ApplicationUserId = "aaa",
                        AccountNumber = "1111111111111111111112",
                        NickName = "Tosho",
                    }
                }

            };

            var receiver = new ApplicationUser()
            {
                Id = "bbb",
                UserName = "bbb@bbb.bbb",
                FirstName = "Haralampi",
                LastName = "Haralampov",
                IsDeleted = false,
                Accounts = new List<Account>()
                {
                    new Account()
                    {
                        Id = new Guid("4e58fbe1-7ad7-4188-aa82-5960ede74979"),
                        Balance = 1,
                        Date = DateTime.Now,
                        ApplicationUserId = "bbb",
                        AccountNumber = "1111111111111111111113",
                        NickName = "Hari",
                    }
                }
            };

            await repo.AddAsync(sender);
            await repo.AddAsync(receiver);
            await repo.AddAsync(user);
            await repo.AddAsync(secondUser);
            await repo.SaveChangesAsync();
        }
    }
}
