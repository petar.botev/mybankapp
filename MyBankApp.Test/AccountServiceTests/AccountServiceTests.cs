﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using MyBankApp.Core.Constants;
using MyBankApp.Core.Services;
using MyBankApp.Core.Services.Contracts;
using MyBankApp.Core.ViewModels;
using MyBankApp.Infrastructure.Entities;
using MyBankApp.Infrastructure.Repositories;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyBankApp.Test.AccountServiceTests
{
    public class AccountServiceTests
    {
        private ServiceProvider serviceProvider;
        private InMemoryDbContext dbContext;

        [SetUp]
        public async Task Setup()
        {
            dbContext = new InMemoryDbContext();
            var serviceCollection = new ServiceCollection();

            serviceProvider = serviceCollection
                .AddSingleton(sp => dbContext.CreateContext())
                .AddSingleton<IApplicatioDbRepository, ApplicatioDbRepository>()
                .AddSingleton<IAccountService, AccountService>()
                .AddSingleton<ICommonService, CommonService>()
                .AddSingleton<IHttpContextAccessor, HttpContextAccessor>()
                .BuildServiceProvider();

            var repo = serviceProvider.GetService<IApplicatioDbRepository>();
            await SeedDbAsync(repo);

        }


        // Create();

        [Test]
        public async Task UnknownUserShouldThrowException()
        {
            var userName = "a";

            var service = serviceProvider.GetService<IAccountService>();

            var res = Assert.CatchAsync<ArgumentException>(async () => await service.Create(userName), "User not found.");
            Assert.AreEqual("User not found.", res.ParamName);

            TearDown();
        }


        [Test]
        public async Task IfUserExistsShouldNotThrowException()
        {
            var userName = "asd@asd.asd";

            var service = serviceProvider.GetService<IAccountService>();

            Assert.DoesNotThrowAsync(async () => await service.Create(userName));

            TearDown();
        }

        [Test]
        public void AccountShouldBeInDb()
        {

            var service = serviceProvider.GetService<IAccountService>();
            service.Create("asd@asd.asd");
            var account = dbContext.CreateContext().Accounts.FirstOrDefault(x => x.NickName == "Pesho Peshev").NickName;

            Assert.IsNotNull(account);
            Assert.AreEqual("Pesho Peshev", account);

            TearDown();
        }


        // GetAll();

        [Test]
        public void ReturnsCollectionOfAccounts()
        {
            var userName = "asd@asd.asd";

            var service = serviceProvider.GetService<IAccountService>();

            var accounts = service.GetAll(userName);

            Assert.AreEqual(1, accounts.Count);

            TearDown();
        }

        // Delete();

        [Test]
        public void ThrowExceptionWhenUserIsNotFound()
        {
            var userName = "a";
            var accountId = "41d3892f-9df5-4787-ada7-38704e5fff41";

            var service = serviceProvider.GetService<IAccountService>();

            var res = Assert.CatchAsync<ArgumentNullException>(async () => await service.Delete(accountId, userName), MessageConstant.UserNullMessage);
            Assert.AreEqual(MessageConstant.UserNullMessage, res.ParamName);

            TearDown();
        }

        [Test]
        public void ThrowExceptionWhenAccountIsNotFound()
        {
            var userName = "asd@asd.asd";
            var accountId = "a";

            var service = serviceProvider.GetService<IAccountService>();

            var res = Assert.CatchAsync<ArgumentNullException>(async () => await service.Delete(accountId, userName), MessageConstant.AccountNullMessage);
            Assert.AreEqual(MessageConstant.AccountNullMessage, res.ParamName);

            TearDown();
        }

        [Test]
        public void IfAccountIsRemovedFromDb()
        {
            var userName = "asd@asd.asd";
            var accountId = "41d3892f-9df5-4787-ada7-38704e5fff41";

            var service = serviceProvider.GetService<IAccountService>();
            var repo = serviceProvider.GetService<IApplicatioDbRepository>();

            service.Delete(accountId, userName);

            var res = repo.All<Account>().Where(x => x.Id.ToString() == accountId).FirstOrDefault();

            Assert.IsNull(res);

            TearDown();
        }

        [Test]
        public async Task GetAccountThrowExceptionWhenAccountIsNull()
        {
            var userId = "asd";
            var accountId = "a";

            var service = serviceProvider.GetService<IAccountService>();

            var res = Assert.CatchAsync<ArgumentNullException>(async () => await service.GetAccount(accountId, userId), "Account not found.");
            Assert.AreEqual(MessageConstant.AccountNullMessage, res.ParamName);

            TearDown();
        }

        [Test]
        public async Task GetAccountReturnsAccountViewModel()
        {
            var userId = "asd";
            var accountId = "41d3892f-9df5-4787-ada7-38704e5fff41";

            var service = serviceProvider.GetService<IAccountService>();

            var model = await service.GetAccount(accountId, userId);

            int? expected = 12;

            Assert.IsInstanceOf<AccountViewModel>(model);
            Assert.AreEqual("1111111111111111111111", model.AccountNumber);
            Assert.AreEqual(expected, model.Balance);
            Assert.Positive(model.Balance);
            Assert.AreEqual("Pesho", model.NickName);
            Assert.AreEqual("asd", model.UserId);

            TearDown();
        }


        // Info();

        [Test]
        public async Task InfoThrowsExceptionWhenUserIsNull()
        {
            var userId = "a";
            var accountId = "41d3892f-9df5-4787-ada7-38704e5fff41";

            var service = serviceProvider.GetService<IAccountService>();

            var res = Assert.CatchAsync<ArgumentNullException>(async () => await service.Info(userId), MessageConstant.UserNullMessage);
            Assert.AreEqual(MessageConstant.UserNullMessage, res.ParamName);

            TearDown();
        }

        [Test]
        public async Task InfoThrowsExceptionWhenUserAccountsCountIsZero()
        {
            var userId = "dsa";

            var service = serviceProvider.GetService<IAccountService>();

            var res = Assert.CatchAsync<ArgumentNullException>(async () => await service.Info(userId), MessageConstant.UserAccountsZeroMessage);
            Assert.AreEqual(MessageConstant.UserAccountsZeroMessage, res.ParamName);

            TearDown();
        }

        [Test]
        public async Task InfoReturnsAllAccountViewModel()
        {
            var userId = "asd";
            var accountId = "41d3892f-9df5-4787-ada7-38704e5fff41";

            var service = serviceProvider.GetService<IAccountService>();

            var model = await service.Info(userId);

            Assert.IsInstanceOf<List<AllAccountsViewModel>>(model);
            Assert.AreEqual(1, model.Count);
            Assert.AreEqual("1111111111111111111111", model.FirstOrDefault().AccountNumber);
            Assert.AreEqual(12, model.FirstOrDefault().Balance);
            Assert.Positive(model.FirstOrDefault().Balance);
            Assert.AreEqual("Pesho", model.FirstOrDefault().NickName);
            Assert.AreEqual("asd", model.FirstOrDefault().User.Id);

            TearDown();
        }

        // Edit();

        [Test]
        public void EditThrowExceptionWhenUserIsNull()
        {
            var userName = "a";
            var model = new AccountViewModel()
            {
                AccountNumber = "12345",
                Balance = 1,
                NickName = "Changed"
            };

            var service = serviceProvider.GetService<IAccountService>();

            var res = Assert.CatchAsync<ArgumentNullException>(async () => await service.Edit(model, userName), MessageConstant.UserNullMessage);
            Assert.AreEqual(MessageConstant.UserNullMessage, res.ParamName);

            TearDown();
        }

        [Test]
        public void EditThrowExceptionWhenAccountIsNull()
        {
            var userName = "dsa@dsa.dsa";
            var model = new AccountViewModel()
            {
                Id = "a",
                AccountNumber = "12345",
                Balance = 1,
                NickName = "Changed"
            };

            var service = serviceProvider.GetService<IAccountService>();

            var res = Assert.CatchAsync<ArgumentNullException>(async () => await service.Edit(model, userName), MessageConstant.AccountNullMessage);
            Assert.AreEqual(MessageConstant.AccountNullMessage, res.ParamName);

            TearDown();
        }

        [Test]
        public async Task EditChangeCorrectlyDataInDb()
        {
            var userName = "asd@asd.asd";
            var model = new AccountViewModel()
            {
                Id = new Guid("41d3892f-9df5-4787-ada7-38704e5fff41").ToString(),
                AccountNumber = "1",
                Balance = 1,
                NickName = "Changed"
            };

            var service = serviceProvider.GetService<IAccountService>();

            var userId = await service.Edit(model, userName);

            var repo = serviceProvider.GetService<IApplicatioDbRepository>();

            var account = repo.All<Account>()
                .FirstOrDefault(x => x.Id.ToString().ToUpper() == model.Id.ToUpper());

            Assert.AreEqual("1", account.AccountNumber);
            Assert.AreEqual(1, account.Balance);
            Assert.AreEqual("Changed", account.NickName);

            TearDown();
        }



        public void TearDown()
        {
            dbContext.Dispose();
        }

        private async Task SeedDbAsync(IApplicatioDbRepository repo)
        {
            var user = new ApplicationUser()
            {
                Id = "asd",
                UserName = "asd@asd.asd",
                FirstName = "Pesho",
                LastName = "Peshev",
                IsDeleted = false,
                Accounts = new List<Account>()
                {
                    new Account()
                    {
                        Id = new Guid("41d3892f-9df5-4787-ada7-38704e5fff41"),
                        Balance = 12,
                        Date = DateTime.Now,
                        ApplicationUserId = "12345",
                        AccountNumber = "1111111111111111111111",
                        NickName = "Pesho",
                    }
                }
            };

            var secondUser = new ApplicationUser()
            {
                Id = "dsa",
                UserName = "dsa@dsa.dsa",
                FirstName = "Gosho",
                LastName = "Geshev",
                IsDeleted = false,
                Accounts = new List<Account>()
            };

            await repo.AddAsync(user);
            await repo.AddAsync(secondUser);
            await repo.SaveChangesAsync();
        }
    }
}
