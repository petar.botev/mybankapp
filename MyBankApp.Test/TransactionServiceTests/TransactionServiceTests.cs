﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using MyBankApp.Core.Constants;
using MyBankApp.Core.Services;
using MyBankApp.Core.Services.Contracts;
using MyBankApp.Core.ViewModels;
using MyBankApp.Infrastructure.Entities;
using MyBankApp.Infrastructure.Repositories;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyBankApp.Test.TransactionServiceTests
{
    public class TransactionServiceTests
    {
        private ServiceProvider serviceProvider;
        private InMemoryDbContext dbContext;

        [SetUp]
        public async Task Setup()
        {
            dbContext = new InMemoryDbContext();
            var serviceCollection = new ServiceCollection();

            serviceProvider = serviceCollection
                .AddSingleton(sp => dbContext.CreateContext())
                .AddSingleton<IApplicatioDbRepository, ApplicatioDbRepository>()
                .AddSingleton<ITransactionService, TransactionService>()
                .AddSingleton<ICommonService, CommonService>()
                .AddSingleton<IHttpContextAccessor, HttpContextAccessor>()
                .BuildServiceProvider();

            var repo = serviceProvider.GetService<IApplicatioDbRepository>();
            await SeedDbAsync(repo);

        }

        // SearchName()

        [Test]
        public void SearchNameReturnsJsonResult()
        {
            var name = "asd";

            var service = serviceProvider.GetService<ITransactionService>();

            var result = service.SearchName(name);

            Assert.IsInstanceOf(typeof(JsonResult), result);

            TearDown();
        }

        // GetReceiver();

        [Test]
        public void GetReceiverThrowsExceptionWhenReceiverIsNull()
        {
            var userId = "a";

            var service = serviceProvider.GetService<ITransactionService>();

            var res = Assert.Catch<ArgumentException>(() => service.GetReceiver(userId), MessageConstant.ReceiverNullException);
            Assert.AreEqual(MessageConstant.ReceiverNullException, res.ParamName);

            TearDown();
        }

        [Test]
        public void ReceiverReturnsShowReceiverViewModel()
        {
            var userId = "asd";

            var service = serviceProvider.GetService<ITransactionService>();

            var result = service.GetReceiver(userId);

            Assert.IsInstanceOf(typeof(ShowReceiverViewModel), result);

            TearDown();
        }

        // Create();

        [Test]
        public void CreateThrowsExceptionWhenSenderIsNull()
        {
            var model = new TransactionInputModel()
            {
                Sum = 1,
                SenderAccountId = new Guid("ba3fa946-8749-4c66-8266-05b8a14e1ae0"),
                ReceiverAccountId = new Guid("4e58fbe1-7ad7-4188-aa82-5960ede74979")
            };

            var senderName = "a";

            var service = serviceProvider.GetService<ITransactionService>();

            var res = Assert.CatchAsync<ArgumentNullException>( async () => await service.Create(model, senderName), MessageConstant.SenderNullException);
            
            Assert.AreEqual(MessageConstant.SenderNullException, res.ParamName);

            TearDown();
        }

        [Test]
        public void CreateThrowsExceptionWhenSenderAccountIdIsNull()
        {
            var model = new TransactionInputModel()
            {
                Sum = 1,
                SenderAccountId = new Guid("00000000-0000-0000-0000-000000000000"),
                ReceiverAccountId = new Guid("4e58fbe1-7ad7-4188-aa82-5960ede74979")
            };

            var senderName = "aaa@aaa.aaa";

            var service = serviceProvider.GetService<ITransactionService>();

            var res = Assert.CatchAsync<ArgumentNullException>(async () => await service.Create(model, senderName), MessageConstant.SenderAccountNullException);

            Assert.AreEqual(MessageConstant.SenderAccountNullException, res.ParamName);

            TearDown();
        }

        [Test]
        public void CreateThrowsExceptionWhenReceiverAccountIdIsNull()
        {
            var model = new TransactionInputModel()
            {
                Sum = 1,
                SenderAccountId = new Guid("ba3fa946-8749-4c66-8266-05b8a14e1ae0"),
                ReceiverAccountId = new Guid("00000000-0000-0000-0000-000000000000")
            };

            var senderName = "aaa@aaa.aaa";

            var service = serviceProvider.GetService<ITransactionService>();

            var res = Assert.CatchAsync<ArgumentNullException>(async () => await service.Create(model, senderName));

            Assert.AreEqual(MessageConstant.ReceiverAccountNullException, res.ParamName);

            TearDown();
        }

        [Test]
        public void CreateThrowsExceptionWhenSenderAccountBalanceIsUnderZero()
        {
            var model = new TransactionInputModel()
            {
                Sum = 11,
                SenderAccountId = new Guid("ba3fa946-8749-4c66-8266-05b8a14e1ae0"),
                ReceiverAccountId = new Guid("4e58fbe1-7ad7-4188-aa82-5960ede74979")
            };

            var senderName = "aaa@aaa.aaa";

            var service = serviceProvider.GetService<ITransactionService>();

            var res = Assert.CatchAsync<ArgumentOutOfRangeException>(async () => await service.Create(model, senderName));

            Assert.AreEqual(MessageConstant.NotEnoughMoneyInAccount, res.ParamName);

            TearDown();
        }

        [Test]
        public void ReceiverBalanceIsIncreased()
        {
            var model = new TransactionInputModel()
            {
                Sum = 5,
                SenderAccountId = new Guid("ba3fa946-8749-4c66-8266-05b8a14e1ae0"),
                ReceiverAccountId = new Guid("4e58fbe1-7ad7-4188-aa82-5960ede74979")
            };

            var receiverName = "bbb@bbb.bbb";

            var service = serviceProvider.GetService<ITransactionService>();

            service.Create(model, receiverName);

            var repo = serviceProvider.GetService<IApplicatioDbRepository>();

            var receiver = repo.All<Account>()
                .FirstOrDefault(x => x.Id.ToString().ToUpper() == "4e58fbe1-7ad7-4188-aa82-5960ede74979".ToUpper());

            Assert.AreEqual(6, receiver.Balance);

            TearDown();
        }

        [Test]
        public void SenderBalanceIsDecrease()
        {
            var model = new TransactionInputModel()
            {
                Sum = 5,
                SenderAccountId = new Guid("ba3fa946-8749-4c66-8266-05b8a14e1ae0"),
                ReceiverAccountId = new Guid("4e58fbe1-7ad7-4188-aa82-5960ede74979")
            };

            var receiverName = "bbb@bbb.bbb";

            var service = serviceProvider.GetService<ITransactionService>();

            service.Create(model, receiverName);

            var repo = serviceProvider.GetService<IApplicatioDbRepository>();

            var sender = repo.All<Account>()
                .FirstOrDefault(x => x.Id.ToString().ToUpper() == "ba3fa946-8749-4c66-8266-05b8a14e1ae0".ToUpper());

            Assert.AreEqual(5, sender.Balance);

            TearDown();
        }

        // All();

        [Test]
        public void AllShouldReturnCollectionOfAllTransactionViewModel()
        {
            var userName = "asd@asd.asd";

            var service = serviceProvider.GetService<ITransactionService>();

            var result = service.All(userName);

            Assert.IsInstanceOf(typeof(ICollection<AllTransactionsViewModel>), result);

            TearDown();
        }

        public void TearDown()
        {
            dbContext.Dispose();
        }

        private async Task SeedDbAsync(IApplicatioDbRepository repo)
        {
            var user = new ApplicationUser()
            {
                Id = "asd",
                UserName = "asd@asd.asd",
                FirstName = "Pesho",
                LastName = "Peshev",
                IsDeleted = false,
                Accounts = new List<Account>()
                {
                    new Account()
                    {
                        Id = new Guid("41d3892f-9df5-4787-ada7-38704e5fff41"),
                        Balance = 12,
                        Date = DateTime.Now,
                        ApplicationUserId = "12345",
                        AccountNumber = "1111111111111111111111",
                        NickName = "Pesho",
                    }
                },
                Transactions = new List<Transaction>()
                {
                    new Transaction()
                    {
                        Sum = 10,
                        Description = "aaa",
                        Date = DateTime.Now,
                        SenderAccountID = new Guid("41d3892f-9df5-4787-ada7-38704e5fff41"),
                        ReceiverAccountID = new Guid("4e58fbe1-7ad7-4188-aa82-5960ede74979"),
                        UserId = "asd"
                    }
                }
            };

            var secondUser = new ApplicationUser()
            {
                Id = "dsa",
                UserName = "dsa@dsa.dsa",
                FirstName = "Gosho",
                LastName = "Geshev",
                IsDeleted = false,
                Accounts = new List<Account>()
            };

            var sender = new ApplicationUser()
            {
                Id = "aaa",
                UserName = "aaa@aaa.aaa",
                FirstName = "Tosho",
                LastName = "Toshev",
                IsDeleted = false,
                Accounts = new List<Account>()
                {
                    new Account()
                    {
                        Id = new Guid("ba3fa946-8749-4c66-8266-05b8a14e1ae0"),
                        Balance = 10,
                        Date = DateTime.Now,
                        ApplicationUserId = "aaa",
                        AccountNumber = "1111111111111111111112",
                        NickName = "Tosho",
                    }
                }

            };

            var receiver = new ApplicationUser()
            {
                Id = "bbb",
                UserName = "bbb@bbb.bbb",
                FirstName = "Haralampi",
                LastName = "Haralampov",
                IsDeleted = false,
                Accounts = new List<Account>()
                {
                    new Account()
                    {
                        Id = new Guid("4e58fbe1-7ad7-4188-aa82-5960ede74979"),
                        Balance = 1,
                        Date = DateTime.Now,
                        ApplicationUserId = "bbb",
                        AccountNumber = "1111111111111111111113",
                        NickName = "Hari",
                    }
                }
            };

            await repo.AddAsync(sender);
            await repo.AddAsync(receiver);
            await repo.AddAsync(user);
            await repo.AddAsync(secondUser);
            await repo.SaveChangesAsync();
        }
    }
}
