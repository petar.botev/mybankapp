﻿using MyBankApp.Core.Services;
using MyBankApp.Core.Services.Contracts;
using MyBankApp.Infrastructure.Repositories;

namespace MyBankApp.Extensions
{
    public static class ServiceCollectionExtension
    {
        public static IServiceCollection ApplicationServices(this IServiceCollection services)
        {
            services.AddScoped<IApplicatioDbRepository, ApplicatioDbRepository>();
            services.AddScoped<IInitialPageService, InitialPageService>();
            services.AddScoped<IAccountService, AccountService>();
            services.AddScoped<ICommonService, CommonService>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<ISoftDeleteService, SoftDeleteService>();
            services.AddScoped<ITransactionService, TransactionService>();
            services.AddScoped<IInitialPageService, InitialPageService>();
            services.AddScoped<IPurchaseService, PurchaseService>();

            return services;
        }
    }
}
