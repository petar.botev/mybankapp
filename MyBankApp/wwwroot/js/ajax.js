﻿$(function () {
    $("#searchName").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "/transaction/autocomplete",
                data: { "prefix": request.term },
                type: "post",
                success: function (data) {
                    response($.map(data, function (item) {
                       
                        return item;
                    }))
                },
                error: function (response) {
                    alert(response.responseText);
                }
            });
        },
        select: function (e, i) {

            $("#searchNameId").val(i.item.val);
        },
        minLength: 1
    });
});
