﻿let button = document.querySelector("#submitButton");
let value = document.querySelector("#searchName");

button.addEventListener("click", function (e) {

    console.log(value.value);

    if (value.value == 0) {
        e.preventDefault();
        return alert("Input can not be empty.");
    }
    if (!isNaN(value.value)) {
        e.preventDefault();
        return alert("Input can not be a number.");
    }
});
