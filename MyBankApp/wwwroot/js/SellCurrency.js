﻿let amount = document.querySelector(".amount");
let sellAmount = document.querySelector(".sellAmount > input");
let priceElement = document.querySelector(".price");
let amountInUsdElement = document.querySelector(".amountInUsd");
let btnElement = document.querySelector(".btn");
let inputElement = document.createElement("input");
let formElement = document.querySelector("form");

amount.addEventListener("click", function (e) {
    e.preventDefault();

    let sellAmountValue = amount.innerHTML;
    sellAmount.value = sellAmountValue;

});

inputElement.setAttribute("Name", "amountInUsd");
amountInUsdElement.appendChild(inputElement);

amount.addEventListener("click", function (e) {
    inputElement.value = parseFloat(amount.innerHTML / priceElement.innerHTML).toFixed(4);
});


sellAmount.addEventListener("keyup", function (e) {
    e.preventDefault();

    inputElement.value = parseFloat(sellAmount.value / priceElement.innerHTML).toFixed(4);

    let isAmountPositive = amount.innerHTML - sellAmount.value;

    if (isAmountPositive < 0) {
        
    }
});

var buttonParent = btnElement.parentElement;

buttonParent.addEventListener("click", function (e) {
    if ((amount.innerHTML - sellAmount.value) < 0) {
        e.stopPropagation();
        
    }
    else {
        amount.innerHTML -= sellAmount.value;
    }
});

