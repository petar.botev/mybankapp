﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MyBankApp.Core.Services.Contracts;
using MyBankApp.Core.ViewModels;

namespace MyBankApp.Controllers
{
    //[Authorize(Roles = "PremiumUser, Administrator")]
    public class PurchaseController : BaseController
    {
        private readonly IPurchaseService service;
        private readonly IAccountService accountService;
        private readonly IHttpContextAccessor context;
        private readonly IInitialPageService initialPageService;

        public PurchaseController(IPurchaseService service, IAccountService accountService, IHttpContextAccessor context, IInitialPageService initialPageService)
        {
            this.service = service;
            this.accountService = accountService;
            this.context = context;
            this.initialPageService = initialPageService;
        }

        public IActionResult SelectAccount(string name, decimal price)
        {
            ViewBag.Name = name;
            ViewBag.Price = price;

            var userName = context.HttpContext.User.Identity.Name;

            var model = accountService.GetAll(userName);
            return View(model);
        }


        public IActionResult Currency(string name, decimal price, string AccountId)
        {
            ViewBag.AccountId = AccountId;

            var model = service.BuyCurrencyView(name, price);

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> BuyCurrency(BuyCurrencyInputModel model)
        {
            if (!ModelState.IsValid)
            {
                
            }
            await service.BuyCurrency(model);
            return RedirectToAction("BoughtCurrencies");
        }

        public async Task<IActionResult> BoughtCurrencies()
        {
            var latestCurrencyValues = await initialPageService.GetCurrencyData();

            var model = await service.BoughtCurrencies(latestCurrencyValues);
            return View(model);
        }

        public async Task<IActionResult> Sell(string name, decimal amount)
        {
            var latestCurrencyValues = await initialPageService.GetCurrencyData();

            var model = await service.SellCurrencyView(name, amount, latestCurrencyValues);
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Sell(SellCurrencyInputModel model)
        {
            try
            {
                var userName = context.HttpContext.User.Identity.Name;
                await service.SellCurrency(model, userName);
                return RedirectToAction(nameof(BoughtCurrencies));
            }
            catch (ArgumentNullException ex)
            {
                ViewBag.Error = ex.Message;

                return View("Error");
            }
        }
    }
}
