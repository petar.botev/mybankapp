﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace MyBankApp.Controllers
{
    [Authorize]
    public class BaseController : Controller
    {
        
    }
}
