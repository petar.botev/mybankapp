﻿using Microsoft.AspNetCore.Mvc;
using MyBankApp.Core.Services.Contracts;

namespace MyBankApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PostApiController : Controller
    {
        private readonly IHttpContextAccessor context;
        private readonly ITransactionService service;

        public PostApiController(ITransactionService service, IHttpContextAccessor context)
        {
            this.service = service;
            this.context = context;
        }

        [Produces("application/json")]
        [HttpGet("search")]
        public JsonResult Search()
        {
            string term = context.HttpContext.Request.Query["term"].ToString();
            var users = service.SearchName(term);
            return Json(users);
        }
    }
}
