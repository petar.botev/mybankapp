﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MyBankApp.Core.Constants;
using MyBankApp.Core.Services.Contracts;

namespace MyBankApp.Controllers
{
    public class AccountController : BaseController
    {
        private readonly IAccountService accountService;
        private readonly IHttpContextAccessor context;


        public AccountController(IAccountService accountService, IHttpContextAccessor context)
        {
            this.accountService = accountService;
            this.context = context;
        }

        [HttpPost]
        public async Task<IActionResult> Create()
        {
            var user = User.Identity.Name;
            await accountService.Create(user);
            return RedirectToAction("All");
        }
        
        public async Task<IActionResult> All()
        {
            var userName = context.HttpContext.User.Identity.Name;

            if (userName == null)
            {
                throw new ArgumentNullException(MessageConstant.UserNullMessage);
            }

            var accountsViewModel = accountService.GetAll(userName);
            return View(accountsViewModel);
        }

        public async Task<IActionResult> Delete(string id)
        {
            var userName = context.HttpContext.User.Identity.Name;

            await accountService.Delete(id, userName);

            return RedirectToAction("All");
        }
    }
}
