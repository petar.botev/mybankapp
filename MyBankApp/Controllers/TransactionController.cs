﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MyBankApp.Core.Services.Contracts;
using MyBankApp.Core.ViewModels;

namespace MyBankApp.Controllers
{
    public class TransactionController : BaseController
    {
        private readonly ITransactionService service;
        private readonly IHttpContextAccessor context;

        public TransactionController(ITransactionService service, IHttpContextAccessor context)
        {
            this.service = service;
            this.context = context;
        }

        public async Task<IActionResult> VerifySendingSum(string sum, string senderAccountId, TransactionInputModel model)
        {
            var res = service.CheckBalance(sum, senderAccountId);
            if (sum == null || model.Description == null)
            {
                return await Create(model);
            }

            if (res.Balance < decimal.Parse(sum))
            {
                return StatusCode(600);
            }
            else
            {
                return await Create(model);
            }
        }

        [HttpPost]
        public JsonResult Autocomplete(string prefix)
        {
            var users = service.SearchName(prefix).Value;
            return Json(users);
        }

        public IActionResult SearchName(string senderAccountId)
        {
            ViewBag.AccountId = senderAccountId;
            return View();
        }

        [HttpPost]
        public IActionResult SearchName(string receiverName, string receiverId, string senderAccountId)
        {
            ViewBag.SenderAccountId = senderAccountId;
            var receiver = service.GetReceiver(receiverId);
            return PartialView("_ReceiverPartial", receiver);
        }

        public IActionResult Create(string id)
        {

            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(TransactionInputModel model)
        {
            
            var senderName = context.HttpContext.User.Identity.Name;

            if (!ModelState.IsValid)
            {
                return View(model);
            }

            await service.Create(model, senderName);

            return Redirect("/Account/All");
        }

        public async Task<IActionResult> All ()
        {
            var currUser = context.HttpContext.User.Identity.Name;

            var model = service.All(currUser);

            return View(model);
        }
    }
}
