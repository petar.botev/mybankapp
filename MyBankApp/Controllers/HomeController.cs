﻿using Microsoft.AspNetCore.Mvc;
using MyBankApp.Core.Services.Contracts;
using MyBankApp.Models;
using System.Diagnostics;

namespace MyBankApp.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IInitialPageService currencyService;

        public HomeController(ILogger<HomeController> logger, IInitialPageService currencyService)
        {
            _logger = logger;
            this.currencyService = currencyService;
        }

        
        public async Task<IActionResult> Index()
        {
            var model = await currencyService.GetCurrencyData();
            return View(model);
        }

        public async Task<IActionResult> Index2()
        {
            var model = await currencyService.GetCurrencyData();
            return View(model);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}