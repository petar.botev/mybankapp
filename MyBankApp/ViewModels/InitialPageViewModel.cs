﻿using System.ComponentModel.DataAnnotations;

namespace MyBankApp.ViewModels
{
    public class InitialPageViewModel
    {
        public string Disclaimer { get; set; }


        public string License { get; set; }


        public decimal Timestamp { get; set; }

        public string Base { get; set; }

        public Dictionary<string, decimal> Rates { get; set; }
    }
}
