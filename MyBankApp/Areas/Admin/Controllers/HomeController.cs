﻿using Microsoft.AspNetCore.Mvc;
using MyBankApp.Core.Services.Contracts;

namespace MyBankApp.Areas.Administration.Controllers
{
    public class HomeController : BaseController
    {
        private readonly IUserService userService;

        public HomeController(IUserService userService)
        {
            this.userService = userService;
        }

        public IActionResult Index()
        {
            var users = userService.GetAll();

            return View(users);
        }
    }
}
