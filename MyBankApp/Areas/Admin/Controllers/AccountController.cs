﻿using Microsoft.AspNetCore.Mvc;
using MyBankApp.Areas.Administration.Controllers;
using MyBankApp.Core.Services.Contracts;
using MyBankApp.Core.ViewModels;

namespace MyBankApp.Areas.Admin.Controllers
{
    public class AccountController : BaseController
    {
        private readonly IAccountService accountService;
        private readonly IHttpContextAccessor context;

        public AccountController(IAccountService accountService, IHttpContextAccessor context)
        {
            this.accountService = accountService;
            this.context = context;
        }

        [HttpGet]
        public async Task<IActionResult> Edit(string id, string userId)
        {
            var model = await accountService.GetAccount(id, userId);
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(AccountViewModel model, string userId)
        {
            var userName = context.HttpContext.User.Identity.Name;

            if (!ModelState.IsValid)
            {
                return View(model);
            }
            await accountService.Edit(model, userName);
            
            return Redirect($"/Admin/User/Info/{userId}");
        }

        public async Task<IActionResult> Delete(string id)
        {
            var userName = context.HttpContext.User.Identity.Name;

            await accountService.Delete(id, userName);

            return Redirect("/Admin/Home/Index");
        }
    }
}
