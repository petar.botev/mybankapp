﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using MyBankApp.Areas.Administration.Controllers;
using MyBankApp.Core.Constants;
using MyBankApp.Core.Services.Contracts;
using MyBankApp.Core.ViewModels;
using MyBankApp.Infrastructure.Entities;

namespace MyBankApp.Areas.Admin.Controllers
{
    public class UserController : BaseController
    {
        private readonly IUserService userService;
        private readonly IAccountService accountService;
        private readonly RoleManager<IdentityRole> roleManager;
        private readonly UserManager<ApplicationUser> userManager;

        public UserController(IUserService userService, IAccountService accountService, RoleManager<IdentityRole> roleManager, UserManager<ApplicationUser> userManager)
        {
            this.userService = userService;
            this.accountService = accountService;
            this.roleManager = roleManager;
            this.userManager = userManager;
        }

        public async Task<IActionResult> Delete(string id)
        {
            try
            {
                var currUser = await userManager.FindByIdAsync(id);
                var role = await userManager.GetRolesAsync(currUser);

                if (role.Contains("Administrator"))
                {
                    throw new ArgumentException(MessageConstant.AdminCantBeDeletedMessage);
                }
            }
            catch (ArgumentException ex)
            {

                return BadRequest(ex.Message);
            }

            await userService.Delete(id);

            return Redirect("/Admin/Home/Index");
        }

        public async Task<IActionResult> Info(string id)
        {


            var model = await accountService.Info(id);
            return View(model);
        }

        public async Task<IActionResult> Edit(string userId)
        {
            var user = await userManager.FindByIdAsync(userId);
            var userRoles = await userManager.GetRolesAsync(user);
            var allRoles = roleManager.Roles
                .Where(r => r.Name != "Administrator")
                .ToList();

            var model = userService.GetUser(userId, user, userRoles, allRoles);
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(EditUserViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var identityUser = await userManager.FindByIdAsync(model.Id);
            var role = await roleManager.FindByNameAsync(model.SelectedRole);

            var userHasRole = await userManager.GetRolesAsync(identityUser);

            if (userHasRole.FirstOrDefault() != null)
            {
                var roleToRemove = await userManager.GetRolesAsync(identityUser);
                await userManager.RemoveFromRoleAsync(identityUser, roleToRemove[0]);

                if (role != null)
                {
                    await userManager.AddToRoleAsync(identityUser, model.SelectedRole);
                }
            }
            else if (role != null)
            {
                await userManager.AddToRoleAsync(identityUser, model.SelectedRole);
            }

            await userService.Edit(model);

            return Redirect("/Admin/Home/Index");
        }

        public async Task<IActionResult> CreateRole()
        {
            //await roleManager.CreateAsync(new IdentityRole()
            //{
            //    Name = "User"
            //});

            //await roleManager.CreateAsync(new IdentityRole()
            //{
            //    Name = "PremiumUser"
            //});

            return Ok();
        }
    }
}
