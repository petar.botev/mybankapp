﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MyBankApp.Core.Constants;

namespace MyBankApp.Areas.Administration.Controllers
{
    [Authorize(Roles = CommonConstants.AdminRole)]
    [Area("Admin")]
    public class BaseController : Controller
    {
    }
}
