﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace MyBankApp.Infrastructure.Migrations
{
    public partial class ChangeSumErrorMessage : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "0559f538-125f-4d29-8d51-9df9c49f567b",
                column: "ConcurrencyStamp",
                value: "9b4c3563-51e0-41cc-82ba-9beb867b7456");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "e2380123-cddd-42f4-ab57-f6df1dccddde",
                column: "ConcurrencyStamp",
                value: "78c1a348-8570-4416-b6e0-cd982b4d7fba");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "04cfa831-2ae3-4c58-b5e0-033473ef0dca",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "be162e1f-3157-4360-aa1b-b77d71726450", "1798a443-e961-4dc7-80e1-61249a78494a" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "50e9088e-f40a-4dec-a329-d7c95e55f4b8",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "2ad78604-edaa-4b62-a00e-7547e72d7960", "cd8b3909-eb59-4e7e-af0f-6796dc30e013" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "0559f538-125f-4d29-8d51-9df9c49f567b",
                column: "ConcurrencyStamp",
                value: "b172e32e-b399-4ecc-a017-8151e3f73fb1");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "e2380123-cddd-42f4-ab57-f6df1dccddde",
                column: "ConcurrencyStamp",
                value: "16c54f45-6d85-48c3-aef8-8a6754615931");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "04cfa831-2ae3-4c58-b5e0-033473ef0dca",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "bbd226b1-369a-4af0-9365-4064361e7c4d", "3784617b-2e8f-4214-9cdc-c20363192603" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "50e9088e-f40a-4dec-a329-d7c95e55f4b8",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "ae7636b8-9e25-4494-823d-8dcad8abfc38", "f059a6ce-c17e-48c5-b110-8a3ef9004ae8" });
        }
    }
}
