﻿namespace MyBankApp.Infrastructure.Entities
{
    public class ApiCurrency
    {
        public Guid Id { get; set; }

        public string Disclaimer { get; set; }


        public string License { get; set; }


        public decimal Timestamp { get; set; }

        public string Base { get; set; }

        public DateTime Date { get; set; } = DateTime.UtcNow;

        public ICollection<Rate> Rates { get; set; } = new List<Rate>();
    }
}
