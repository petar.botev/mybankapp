﻿using System.ComponentModel.DataAnnotations.Schema;

namespace MyBankApp.Infrastructure.Entities
{
    public class UserRate
    {
        public string UserId { get; set; }

        [ForeignKey(nameof(UserId))]
        public ApplicationUser User { get; set; }

        public Guid RateId { get; set; }

        [ForeignKey(nameof(RateId))]
        public Rate Rate { get; set; }
    }
}
