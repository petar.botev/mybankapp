﻿using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;

namespace MyBankApp.Infrastructure.Entities
{
    public class ApplicationUser : IdentityUser
    {
        [Required]
        [StringLength(20, MinimumLength = 1)]
        public string FirstName { get; set; }

        [Required]
        [StringLength(20, MinimumLength = 2)]
        public string LastName { get; set; }

        [Required]
        public bool IsDeleted { get; set; }

        public ICollection<Account> Accounts { get; set; } = new List<Account>();

        public ICollection<Transaction> Transactions { get; set; } = new List<Transaction>();

        public ICollection<UserRate> UserRates { get; set; } = new List<UserRate>();
    }
}
