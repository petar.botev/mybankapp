﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MyBankApp.Infrastructure.Entities
{
    public class Account
    {
        public Account()
        {
            this.Id = Guid.NewGuid();
            this.Balance = 1000;
        }

        [Key]
        public Guid Id { get; set; }

        [StringLength(22, MinimumLength = 22)]
        public string AccountNumber { get; set; }

        [MinLength(3)]
        [MaxLength(35)]
        public string NickName { get; set; }

        [Required]
        [Column(TypeName = "decimal(18,2)")]
        [Range(0, Double.PositiveInfinity)]
        public decimal Balance { get; set; }

        [Required]
        public DateTime Date { get; set; }

        [Required]
        [ForeignKey(nameof(User))]
        public string ApplicationUserId { get; set; }

        public ApplicationUser User { get; set; }


        public ICollection<Transaction> SenderTransactions { get; set; }

        public ICollection<Transaction> ReceiverTransactions { get; set; }
    }
}
