﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MyBankApp.Infrastructure.Entities
{
    public class Transaction
    {
        public Guid Id { get; set; } = Guid.NewGuid();

        [Required(ErrorMessage = "Sum is required")]
        [Range(1,100000,ErrorMessage = "Sumata ne se poddyrja.")]
        public decimal? Sum { get; set; }

        public DateTime Date { get; set; } = DateTime.Now;

        [MaxLength(50)]
        public string Description { get; set; }

        public Guid SenderAccountID { get; set; }

        [ForeignKey("SenderAccountID")]
        [InverseProperty("SenderTransactions")]
        public Account SenderAccount { get; set; }


        public Guid ReceiverAccountID { get; set; }

        [ForeignKey("ReceiverAccountID")]
        [InverseProperty("ReceiverTransactions")]
        public Account ReceiverAccount { get; set; }


        public string UserId { get; set; }

        [ForeignKey("UserId")]
        public ApplicationUser User { get; set; }
    }
}
