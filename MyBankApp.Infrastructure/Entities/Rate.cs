﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MyBankApp.Infrastructure.Entities
{
    public class Rate
    {
        public Guid Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        [Column(TypeName = "decimal(18,6)")]
        public decimal Percent { get; set; }

        [Column(TypeName = "decimal(18,6)")]
        public decimal Amount { get; set; }

        public Guid ApiCurrencyId { get; set; }

        [Required]
        [ForeignKey(nameof(ApiCurrencyId))]
        public ApiCurrency ApiCurrency { get; set; }

        public ICollection<UserRate> UserRates { get; set; } = new List<UserRate>();

    }
}
