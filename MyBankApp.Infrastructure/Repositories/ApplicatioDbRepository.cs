﻿using MyBankApp.Infrastructure.Common;
using MyBankApp.Infrastructure.Data;

namespace MyBankApp.Infrastructure.Repositories
{
    public class ApplicatioDbRepository : Repository, IApplicatioDbRepository
    {
        public ApplicatioDbRepository(ApplicationDbContext context)
        {
            this.Context = context;
        }
    }
}
