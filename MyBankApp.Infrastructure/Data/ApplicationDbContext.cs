﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using MyBankApp.Infrastructure.Entities;
using Newtonsoft.Json;

namespace MyBankApp.Infrastructure.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {

        }

        public DbSet<ApplicationUser> ApplicationUsers { get; set; }
        public DbSet<Account> Accounts { get; set; }
        public DbSet<Transaction> Transactions { get; set; }
        public DbSet<ApiCurrency> ApiCurrencies { get; set; }
        public DbSet<Rate> Rates { get; set; }
        public DbSet<UserRate> UserRates { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            LoadJson(builder);

            builder.Entity<Transaction>()
                 .HasOne(tr => tr.SenderAccount)
                 .WithMany(a => a.SenderTransactions)
                 .HasForeignKey(tr => tr.SenderAccountID)
                 .OnDelete(DeleteBehavior.Restrict)
                 .IsRequired();

            builder.Entity<Transaction>()
                .HasOne(tr => tr.ReceiverAccount)
                .WithMany(a => a.ReceiverTransactions)
                .HasForeignKey(tr => tr.ReceiverAccountID)
                .OnDelete(DeleteBehavior.Restrict)
                .IsRequired();

            builder.Entity<Transaction>()
                .HasOne(tr => tr.User)
                .WithMany(u => u.Transactions)
                .HasForeignKey(tr => tr.UserId)
                .OnDelete(DeleteBehavior.Restrict)
                .IsRequired();

            builder.Entity<UserRate>()
                .HasKey(x => new { x.UserId, x.RateId });

            builder.Entity<UserRate>()
                .HasOne(x => x.Rate)
                .WithMany(x => x.UserRates)
                .HasForeignKey(x => x.RateId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.Entity<UserRate>()
               .HasOne(x => x.User)
               .WithMany(x => x.UserRates)
               .HasForeignKey(x => x.UserId)
               .OnDelete(DeleteBehavior.Restrict);

            base.OnModelCreating(builder);
        }

        private void Seed(ModelBuilder builder)
        {
            //Seeding a  'Administrator' role to AspNetRoles table
            builder.Entity<IdentityRole>().HasData(new IdentityRole 
            {
                Id = "2c5e174e-3b0e-446f-86af-483d56fd7210", 
                Name = "Administrator", 
                NormalizedName = "ADMINISTRATOR".ToUpper(),
            });


            //a hasher to hash the password before seeding the user to the db
            var hasher = new PasswordHasher<ApplicationUser>();


            //Seeding the User to AspNetUsers table
            builder.Entity<ApplicationUser>().HasData(
                new ApplicationUser
                {
                    Id = "8e445865-a24d-4543-a6c6-9443d048cdb9", // primary key
                    FirstName = "Pesho",
                    LastName = "Peshev",
                    UserName = "asd@asd.asd",
                    NormalizedUserName = "asd@asd.asd".ToUpper(),
                    Email = "asd@asd.asd",
                    PasswordHash = hasher.HashPassword(null, "asdasd"),
                    LockoutEnabled = true,
                    
                }
            );


            //Seeding the relation between our user and role to AspNetUserRoles table
            builder.Entity<IdentityUserRole<string>>().HasData(
                new IdentityUserRole<string>
                {
                    UserId = "8e445865-a24d-4543-a6c6-9443d048cdb9",
                    RoleId = "2c5e174e-3b0e-446f-86af-483d56fd7210",
                }
            );
        }

        private void LoadJson(ModelBuilder builder)
        {
            if (File.Exists(@"..\MyBankApp.Infrastructure\JsonFiles\User.json"))
            {
                var users = JsonConvert.DeserializeObject<ApplicationUser[]>
                   (File.ReadAllText(@"..\MyBankApp.Infrastructure\JsonFiles\User.json"));

                builder.Entity<ApplicationUser>().HasData(users);
            }

            if (File.Exists(@"..\MyBankApp.Infrastructure\JsonFiles\Roles.json"))
            {
                var roles = JsonConvert.DeserializeObject<IdentityRole[]>
                   (File.ReadAllText(@"..\MyBankApp.Infrastructure\JsonFiles\Roles.json"));
               
                builder.Entity<IdentityRole>().HasData(roles);
            }

            if (File.Exists(@"..\MyBankApp.Infrastructure\JsonFiles\UsersRoles.json"))
            {
                var usersRoles = JsonConvert.DeserializeObject<IdentityUserRole<string>[]>
                   (File.ReadAllText(@"..\MyBankApp.Infrastructure\JsonFiles\UsersRoles.json"));

                builder.Entity<IdentityUserRole<string>>().HasData(usersRoles);
            }
        }
    }
}