﻿namespace MyBankApp.Core.ViewModels
{
    public class PaymentInputModel
    {
        public string? Id { get; set; }
        public string? UserName { get; set; }
        public string? Account { get; set; }
        public decimal Sum { get; set; }
    }
}
