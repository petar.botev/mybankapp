﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MyBankApp.Core.ViewModels
{
    public class AccountViewModel
    {
        public string Id { get; set; }

        [StringLength(22, MinimumLength = 22)]
        public string AccountNumber { get; set; }

        [MinLength(3)]
        [MaxLength(35)]
        public string NickName { get; set; }

        [Required]
        [Column(TypeName = "decimal(18,2)")]
        [Range(0, Double.PositiveInfinity)]
        public decimal Balance { get; set; } = 1000;

        public string UserId { get; set; }
    }
}
