﻿using System.ComponentModel.DataAnnotations;

namespace MyBankApp.Core.ViewModels
{
    public class EditUserInputModel
    {
        public string Id { get; set; }

        [Required]
        [StringLength(10, MinimumLength = 1)]
        public string FirstName { get; set; }

        [Required]
        [StringLength(10, MinimumLength = 2)]
        public string LastName { get; set; }

        [EmailAddress]
        public string Email { get; set; }

        public int NumberAccounts { get; set; }

        public bool IsDeleted { get; set; }
    }
}
