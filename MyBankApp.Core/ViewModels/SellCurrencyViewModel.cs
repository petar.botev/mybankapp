﻿namespace MyBankApp.Core.ViewModels
{
    public class SellCurrencyViewModel
    {
        public string Name { get; set; }
        public decimal Amount { get; set; }
        public decimal Price { get; set; }
    }
}
