﻿namespace MyBankApp.Core.ViewModels
{
    public class TransactionFormViewModel
    {
        public Guid Id { get; set; }
        public string? FullName { get; set; }
        public string? Account { get; set; }
    }
}
