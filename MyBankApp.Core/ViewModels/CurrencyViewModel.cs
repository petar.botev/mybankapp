﻿using MyBankApp.Infrastructure.Entities;

namespace MyBankApp.Core.ViewModels
{
    public class CurrencyViewModel
    {
        public string Base { get; set; }
        public ICollection<Rate> Rates { get; set; }
    }
}
