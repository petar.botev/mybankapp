﻿using MyBankApp.Infrastructure.Entities;
using System.ComponentModel.DataAnnotations;

namespace MyBankApp.Core.ViewModels
{
    public class AllTransactionsViewModel
    {
        public Guid Id { get; set; }

        [Required]
        [Range(1, 100000)]
        public decimal? Sum { get; set; }

        public DateTime Date { get; set; }

        [MaxLength(50)]
        public string Description { get; set; }

        public string ReceiverAccount { get; set; }

        public string ReceiverName { get; set; }

        public string SenderAccount { get; set; }

        public ApplicationUser Receiver { get; set; }

        public bool IsOutTransaction { get; set; }
    }
}
