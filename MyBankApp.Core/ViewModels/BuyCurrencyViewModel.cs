﻿namespace MyBankApp.Core.ViewModels
{
    public class BuyCurrencyViewModel
    {
        public string Name { get; set; }
        public decimal Price { get; set; }
    }
}
