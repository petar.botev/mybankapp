﻿namespace MyBankApp.Core.ViewModels
{
    public class UserCurrenciesViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public decimal Amount { get; set; }
        public decimal BoughtPrice { get; set; }
        public decimal CurrentPrice { get; set; }

    }
}
