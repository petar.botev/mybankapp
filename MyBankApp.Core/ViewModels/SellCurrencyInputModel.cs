﻿using System.ComponentModel.DataAnnotations;

namespace MyBankApp.Core.ViewModels
{
    public class SellCurrencyInputModel
    {
        public string Name { get; set; }

        [Range(0.0001, int.MaxValue)]
        public decimal SellAmount { get; set; }

        [Range(0.0001, int.MaxValue)]

        public decimal AmountInUsd { get; set; }

        [Range(0.0001, int.MaxValue)]
        public decimal Ammount { get; set; }

        [Range(0.0001, int.MaxValue)]
        public decimal ChangedAmount { get; set; }
    }
}
