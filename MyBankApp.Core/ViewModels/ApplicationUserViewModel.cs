﻿namespace MyBankApp.Core.ViewModels
{
    public class ApplicationUserViewModel
    {
        public string Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public int NumberAccounts { get; set; }
        public bool IsDeleted { get; set; }

    }
}
