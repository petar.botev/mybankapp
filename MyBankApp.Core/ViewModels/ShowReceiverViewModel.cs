﻿using MyBankApp.Infrastructure.Entities;
namespace MyBankApp.Core.ViewModels
{
    public class ShowReceiverViewModel
    {
        public string? Id { get; set; }

        public string? UserName { get; set; }

        public ICollection<Account>? Accounts { get; set; }
    }
}
