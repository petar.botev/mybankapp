﻿using System.ComponentModel.DataAnnotations;

namespace MyBankApp.Core.ViewModels
{
    public class BuyCurrencyInputModel
    {
        public string Name { get; set; }
        
        public decimal Price { get; set; }

        [Range(0.1, int.MaxValue, ErrorMessage = "Only positive number allowed")]
        public int Amount { get; set; }
        public decimal TotalPrice { get; set; }
        public string UserAccountId { get; set; }

    }
}
