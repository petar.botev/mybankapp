﻿namespace MyBankApp.Core.ViewModels
{
    public class SearchViewModel
    {
        public string Id { get; set; }
        public string FullName { get; set; }
    }
}
