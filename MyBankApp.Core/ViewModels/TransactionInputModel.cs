﻿using System.ComponentModel.DataAnnotations;

namespace MyBankApp.Core.ViewModels
{
    public class TransactionInputModel
    {
        public Guid Id { get; set; }

        [Range(1, 100000, ErrorMessage = "Sumata ne se poddyrja.")]
        [Required]
        public decimal Sum { get; set; }

        [MaxLength(50)]
        [MinLength(3)]
        public string Description { get; set; }

        public Guid SenderId { get; set; }

        public Guid ReceiverId { get; set; }

        public Guid SenderAccountId { get; set; }

        public Guid ReceiverAccountId { get; set; }
    }
}
