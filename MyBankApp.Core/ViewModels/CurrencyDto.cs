﻿namespace MyBankApp.Core.ViewModels
{
    public class CurrencyDto
    {
        public Guid Id { get; set; }

        public string Disclaimer { get; set; }


        public string License { get; set; }


        public decimal Timestamp { get; set; }

        public string Base { get; set; }

        public Dictionary<string, decimal> Rates { get; set; }
    }
}
