﻿namespace MyBankApp.Core.Constants
{
    public static class MessageConstant
    {
        public const string ErrorMessage = "ErrorMessage";
        public const string SuccessMessage = "SuccessMessage";
        public const string WarningMessage = "WarningMessage";

        public const string UserAccountsZeroMessage = "User does not have accounts.";
        public const string UserNullMessage = "User not found.";
        public const string AccountNullMessage = "Account not found.";
        public const string ReceiverNullException = "Receiver not found.";
        public const string SenderNullException = "Sender not found.";
        public const string SenderAccountNullException = "Sender account not found.";
        public const string ReceiverAccountNullException = "Receiver account not found.";
        public const string NotEnoughMoneyInAccount = "The account does not have enough money!";
        public const string UsersZeroMessage = "There are no users.";
        public const string CurrencyNullMessage = "Currency is not found.";
        public const string NotEnoughCurrencyMessage = "Do not have enough currency to sell.";
        public const string AdminCantBeDeletedMessage = "Administrator can not be deleted.";



    }
}
