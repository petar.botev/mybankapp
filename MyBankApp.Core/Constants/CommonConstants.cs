﻿namespace MyBankApp.Core.Constants
{
    public static class CommonConstants
    {
        public const string AdminRole = "Administrator";
        public const string UserRole = "User";
        public const string PremiumUserRole = "PremiumUser";

    }
}
