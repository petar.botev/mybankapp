﻿using MyBankApp.Core.ViewModels;

namespace MyBankApp.Core.Services.Contracts
{
    public interface IPurchaseService
    {
        Task<ICollection<UserCurrenciesViewModel>> BoughtCurrencies(CurrencyViewModel currencyModel);
        Task BuyCurrency(BuyCurrencyInputModel model);
        BuyCurrencyViewModel BuyCurrencyView(string name, decimal price);
        Task SellCurrency(SellCurrencyInputModel model, string userName);
        Task<SellCurrencyViewModel> SellCurrencyView(string name, decimal amount, CurrencyViewModel currencyModel);
    }
}