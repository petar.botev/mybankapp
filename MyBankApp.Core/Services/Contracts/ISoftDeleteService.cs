﻿namespace MyBankApp.Core.Services.Contracts
{
    public interface ISoftDeleteService
    {
        bool IsUserDeleted(string userEmail);
    }
}