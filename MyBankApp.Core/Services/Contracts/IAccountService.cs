﻿using MyBankApp.Core.ViewModels;

namespace MyBankApp.Core.Services.Contracts
{
    public interface IAccountService
    {
        Task Create(string userId);
        Task Delete(string id, string userName);
        Task<string> Edit(AccountViewModel model, string userName);
        Task<AccountViewModel> GetAccount(string accountId, string userId);
        ICollection<UserAccountsViewModel> GetAll(string userName);
        Task<ICollection<AllAccountsViewModel>> Info(string userId);
    }
}