﻿namespace MyBankApp.Core.Services.Contracts
{
    public interface ICommonService
    {
        string GenerateAccountNumber();
    }
}
