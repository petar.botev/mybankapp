﻿using Microsoft.AspNetCore.Mvc;
using MyBankApp.Core.ViewModels;
using MyBankApp.Infrastructure.Entities;

namespace MyBankApp.Core.Services.Contracts
{
    public interface ITransactionService
    {
        ICollection<AllTransactionsViewModel> All(string currUser);
        Account CheckBalance(string sum, string senderAccountId);
        Task Create(TransactionInputModel model, string senderName);
        ShowReceiverViewModel GetReceiver(string userId);
        JsonResult SearchName(string search);
    }
}