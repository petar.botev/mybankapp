﻿using MyBankApp.Core.ViewModels;
using MyBankApp.Infrastructure.Entities;

namespace MyBankApp.Core.Services.Contracts
{
    public interface IInitialPageService
    {
        Task<CurrencyViewModel> GetCurrencyData();
    }
}