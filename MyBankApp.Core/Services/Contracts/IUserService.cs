﻿using Microsoft.AspNetCore.Identity;
using MyBankApp.Core.ViewModels;
using MyBankApp.Infrastructure.Entities;

namespace MyBankApp.Core.Services.Contracts
{
    public interface IUserService
    {
        Task Delete(string id);
        Task Edit(EditUserViewModel model);
        List<ApplicationUserViewModel> GetAll();
        EditUserViewModel GetUser(string userId, ApplicationUser user, IList<string> roles, IList<IdentityRole> allRoles);
    }
}