﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MyBankApp.Core.Constants;
using MyBankApp.Core.Services.Contracts;
using MyBankApp.Core.ViewModels;
using MyBankApp.Infrastructure.Entities;
using MyBankApp.Infrastructure.Repositories;

namespace MyBankApp.Core.Services
{
    public class TransactionService : ITransactionService
    {
        private readonly IApplicatioDbRepository repo;
        private readonly IHttpContextAccessor context;

        public TransactionService(IApplicatioDbRepository repo, IHttpContextAccessor context)
        {
            this.repo = repo;
            this.context = context;
        }

        //public string CheckBalance(string sum, string senderAccountId)
        //{
        //    var senderAccount = repo.All<Account>()
        //        .Where(a => a.Id.ToString().ToLower() == senderAccountId.ToLower())
        //        .FirstOrDefault();

        //    var accountBalance = senderAccount.Balance;

        //    if (sum == null)
        //    {
        //        return null;
        //    }

        //    if (decimal.Parse(sum) > accountBalance)
        //    {
        //        return false.ToString();
        //    }
        //    else
        //    {
        //        return true.ToString();
        //    }
        //}

        public Account CheckBalance(string sum, string senderAccountId)
        {
            var senderAccount = repo.All<Account>()
                .Where(a => a.Id.ToString().ToLower() == senderAccountId.ToLower())
                .FirstOrDefault();

            return senderAccount;
        }

        public JsonResult SearchName(string search)
        {
            var users = repo.All<ApplicationUser>()
                .Where(x => x.UserName.StartsWith(search))
                .OrderBy(x => x.UserName)
                .Select(x => new
                { 
                    label = x.UserName,
                    val = x.Id
                })
                .ToList();

            return new JsonResult(users);
        }

        public ShowReceiverViewModel GetReceiver(string receiverId)
        {
            var receiver = repo.All<ApplicationUser>()
                .Where(x => x.Id == receiverId)
                .Include(x => x.Accounts)
                .Select(x => new ShowReceiverViewModel()
                {
                    Id = x.Id,
                    UserName = x.FirstName + " " + x.LastName,
                    Accounts = x.Accounts
                })
                .FirstOrDefault();

            if (receiver == null)
            {
                throw new ArgumentNullException(MessageConstant.ReceiverNullException);
            }

            return receiver;
        }

        public async Task Create(TransactionInputModel model, string senderName)
        {
            

            var sender = repo.All<ApplicationUser>()
               .Where(x => x.UserName == senderName)
               .FirstOrDefault();

            if (sender == null)
            {
                throw new ArgumentNullException(MessageConstant.SenderNullException);
            }

            var receiverAccount = repo.All<Account>()
                .Where(x => x.Id == model.ReceiverAccountId)
                .FirstOrDefault();

            if (receiverAccount == null)
            {
                throw new ArgumentNullException(MessageConstant.ReceiverAccountNullException);
            }

            var senderAccount = repo.All<Account>()
                .Where(x => x.Id == model.SenderAccountId)
                .FirstOrDefault();

            if (senderAccount == null)
            {
                throw new ArgumentNullException(MessageConstant.SenderAccountNullException);
            }

            if (senderAccount.Balance >= model.Sum)
            {
                receiverAccount.Balance += model.Sum;
                senderAccount.Balance -= model.Sum;
            }
            else
            {
                throw new ArgumentOutOfRangeException(MessageConstant.NotEnoughMoneyInAccount);
            }

            string? senderId = sender.Id;

            var transaction = new Transaction
            {
               Sum = model.Sum,
               Description = model.Description,
               ReceiverAccount = receiverAccount,
               SenderAccount = senderAccount,
               User = sender
            };

             await repo.AddAsync(transaction);
             repo.SaveChanges();
        }

        public ICollection<AllTransactionsViewModel> All(string currUser)
        {
            

            var transactions = repo.All<Transaction>()
                .Where(t => t.SenderAccount.User.UserName == currUser || t.ReceiverAccount.User.UserName == currUser)
                .Select(t => new AllTransactionsViewModel
                {
                    Id = t.Id,
                    Sum = t.Sum,
                    Description = t.Description,
                    Date = t.Date,
                    ReceiverName = t.ReceiverAccount.User.FirstName + " " + t.ReceiverAccount.User.LastName,
                    ReceiverAccount = t.ReceiverAccount.AccountNumber,
                    SenderAccount = t.SenderAccount.AccountNumber,
                    Receiver = t.ReceiverAccount.User,
                    IsOutTransaction = t.SenderAccount.User.UserName == currUser ? true : false
                })
                .OrderByDescending(t => t.Date)
                .ToList();

            return transactions;
        }
    }
}
