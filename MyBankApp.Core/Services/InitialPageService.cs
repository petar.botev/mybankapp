﻿using Microsoft.EntityFrameworkCore;
using MyBankApp.Core.Services.Contracts;
using MyBankApp.Core.ViewModels;
using MyBankApp.Infrastructure.Entities;
using MyBankApp.Infrastructure.Repositories;
using Newtonsoft.Json;

namespace MyBankApp.Core.Services
{
    public class InitialPageService : IInitialPageService
    {
        private readonly IApplicatioDbRepository repo;
        private readonly IHttpClientFactory clientService;

        public InitialPageService(IHttpClientFactory clientService, IApplicatioDbRepository repo)
        {
            this.clientService = clientService;
            this.repo = repo;
        }

        public async Task<CurrencyViewModel> GetCurrencyData()
        {
            var client = clientService.CreateClient();
            HttpResponseMessage response = await client
                .GetAsync("https://openexchangerates.org/api/latest.json?app_id=31e08b1c25fe45438d34eca934b11d67");
            CurrencyDto? model = null;

            if (response.IsSuccessStatusCode)
            {
                var jsonString = await response.Content.ReadAsStringAsync();

                model = JsonConvert.DeserializeObject<CurrencyDto>(jsonString);

                Dictionary<string, decimal> currenciesToUse = model
                    .Rates
                    .Where(x => x.Key == "BGN"
                    || x.Key == "EUR"
                    || x.Key == "CHF"
                    || x.Key == "RUB"
                    || x.Key == "CNY"
                    || x.Key == "JPY"
                    || x.Key == "SEK")
                    .ToDictionary(k => k.Key, v => v.Value);

                model.Rates = currenciesToUse;

                var currency = new ApiCurrency
                {
                    Base = model.Base,
                    Disclaimer = model.Disclaimer,
                    License = model.License,
                    Timestamp = model.Timestamp
                };

                var ratesCollection = currenciesToUse
                    .Select(x => new Rate
                    {
                        Name = x.Key,
                        Percent = x.Value,
                        ApiCurrency = currency
                    })
                    .ToList();

                currency.Rates = ratesCollection;

                var previousCurrency = repo.All<ApiCurrency>()
                    .OrderByDescending(x => x.Date)
                    .Include(x => x.Rates)
                    .FirstOrDefault();
                    

                var currencyViewModel = new CurrencyViewModel
                {
                    Base = currency.Base,
                    Rates = currency.Rates
                };

                if (previousCurrency == null || previousCurrency.Rates.FirstOrDefault().Percent != currency.Rates.FirstOrDefault().Percent)
                {
                    await repo.AddAsync(currency);
                    await repo.SaveChangesAsync();
                }

                return currencyViewModel;
            }
            else
            {
                throw new ArgumentNullException("No connection to the api");
            }
        }
    }
}
