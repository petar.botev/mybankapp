﻿using MyBankApp.Core.Services.Contracts;
using System.Text;

namespace MyBankApp.Core.Services
{
    public class CommonService : ICommonService
    {
        public string GenerateAccountNumber()
        {
            var rand = new Random();
            int firstTwoDigits = rand.Next(10, 99);

            StringBuilder strb = new StringBuilder();
            for (int i = 0; i < 14; i++)
            {
                strb.Append(rand.Next(0, 10));
            }

            string accountNumber = $"BG{firstTwoDigits}MBBA{strb.ToString()}";
            return accountNumber;
        }
    }
}
