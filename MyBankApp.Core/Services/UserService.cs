﻿using Microsoft.AspNetCore.Identity;
using MyBankApp.Core.Constants;
using MyBankApp.Core.Services.Contracts;
using MyBankApp.Core.ViewModels;
using MyBankApp.Infrastructure.Entities;
using MyBankApp.Infrastructure.Repositories;

namespace MyBankApp.Core.Services
{
    public class UserService : IUserService
    {
        private readonly IApplicatioDbRepository repo;

        public UserService(IApplicatioDbRepository repo)
        {
            this.repo = repo;
        }

        public List<ApplicationUserViewModel> GetAll()
        {
            var users = repo.All<ApplicationUser>()
                //.Where(x => x.IsDeleted == false)
                .Select(x => new ApplicationUserViewModel
                {
                    Id = x.Id,
                    FirstName = x.FirstName,
                    LastName = x.LastName,
                    Email = x.Email,
                    NumberAccounts = x.Accounts.Count,
                    IsDeleted = x.IsDeleted
                })
                .OrderBy(x => x.FirstName)
                .ThenBy(x => x.LastName)
                .ToList();
           
            return users;
        }

        public async Task Delete(string id)
        {
            
            var userToDelete = repo.All<ApplicationUser>()
                .FirstOrDefault(x => x.Id == id);

            if (userToDelete == null)
            {
                throw new ArgumentNullException(MessageConstant.UserNullMessage);
            }

            userToDelete.IsDeleted = true;

            repo.Update<ApplicationUser>(userToDelete);

            await repo.SaveChangesAsync();
        }

        public EditUserViewModel GetUser(string userId, ApplicationUser user, IList<string> roles, IList<IdentityRole> allRoles)
        {
            var userModel = new EditUserViewModel
            {
                Id = user.Id,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Email = user.Email,
                IsDeleted = user.IsDeleted,
                SelectedRole = roles.FirstOrDefault(),
                Roles = allRoles.ToList(),
            };

            return userModel;
        }

        public async Task Edit(EditUserViewModel model)
        {
            var user = await repo.GetByIdAsync<ApplicationUser>(model.Id);

            if (user == null)
            {
                throw new ArgumentNullException(MessageConstant.UserNullMessage);
            }

            user.FirstName = model.FirstName;
            user.LastName = model.LastName;
            user.Email = model.Email;
            user.IsDeleted = model.IsDeleted;

            await repo.SaveChangesAsync();
        }
    }
}
