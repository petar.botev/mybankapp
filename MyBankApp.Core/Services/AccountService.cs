﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using MyBankApp.Core.Constants;
using MyBankApp.Core.Services.Contracts;
using MyBankApp.Core.ViewModels;
using MyBankApp.Infrastructure.Data;
using MyBankApp.Infrastructure.Entities;
using MyBankApp.Infrastructure.Repositories;
using System.Linq;

namespace MyBankApp.Core.Services
{
    public class AccountService : IAccountService
    {
        private readonly IApplicatioDbRepository repo;
        private readonly ICommonService commonService;
        private readonly IHttpContextAccessor context;

        public AccountService(IApplicatioDbRepository repo, ICommonService commonService, IHttpContextAccessor context)
        {
            this.repo = repo;
            this.commonService = commonService;
            this.context = context;
        }

        public async Task Create(string userName)
        {
            var user = repo.All<ApplicationUser>(x => x.UserName == userName).FirstOrDefault();

            if (user == null)
            {
                throw new ArgumentNullException(MessageConstant.UserNullMessage);
            }
                
            var accountNumber = commonService.GenerateAccountNumber();

            var account = new Account
            {
                AccountNumber = accountNumber,
                NickName = $"{user.FirstName} {user.LastName}",
                Date = DateTime.Now,
                ApplicationUserId = user.Id
            };
            
            await repo.AddAsync(account);
            await repo.SaveChangesAsync();
        }

        public ICollection<UserAccountsViewModel> GetAll(string userName) 
        {
            var userAccounts = repo.All<ApplicationUser>()
                .Where(x => x.UserName == userName)
                .SelectMany(x => x.Accounts.Select(a => new UserAccountsViewModel
                {
                    Id = a.Id.ToString(),
                    AccountNumber = a.AccountNumber,
                    Balance = a.Balance,
                    Date = a.Date
                }))
                .ToList();

            return userAccounts;
        }

        public async Task Delete(string id, string userName)
        {
            var user = repo.All<ApplicationUser>()
                .Where(x => x.UserName == userName)
                .Include(x => x.Accounts)
                .FirstOrDefault();

            if (user == null)
            {
                throw new ArgumentNullException(MessageConstant.UserNullMessage);
            }

            var account = user.Accounts
                .Where(a => a.Id.ToString().ToUpper() == id.ToUpper())
                .FirstOrDefault();

            if (account == null)
            {
                throw new ArgumentNullException(MessageConstant.AccountNullMessage);
            }

            //account.ApplicationUserId = null;

            repo.Delete<Account>(account);
            await repo.SaveChangesAsync();
        }

        public async Task<AccountViewModel> GetAccount(string accountId, string userId)
        {
            var account = await repo.All<Account>()
                .FirstOrDefaultAsync(x => x.Id.ToString().ToUpper() == accountId.ToUpper());

            if (account == null)
            {
                throw new ArgumentNullException(MessageConstant.AccountNullMessage);
            }

            var accountModel = new AccountViewModel
            {
                AccountNumber = account.AccountNumber,
                Balance = account.Balance,
                NickName = account.NickName,
                UserId = userId
            };

            return accountModel;
        }

        public async Task<ICollection<AllAccountsViewModel>> Info(string userId)
        {
            var user = await repo.All<ApplicationUser>()
                .Where(x => x.Id == userId)
                .Include(x => x.Accounts)
                .FirstOrDefaultAsync();

            if (user == null)
            {
                throw new ArgumentNullException(MessageConstant.UserNullMessage);
            }

            if (user.Accounts.Count == 0)
            {
                throw new ArgumentNullException(MessageConstant.UserAccountsZeroMessage);
            }
            var userAccounts = user.Accounts.Select(x => new AllAccountsViewModel
            {
                 Id = x.Id.ToString(),
                 NickName = x.NickName,
                 AccountNumber = x.AccountNumber,
                 Balance = x.Balance,
                 Date = x.Date,
                 User = user
            })
                .OrderByDescending(x => x.Date)
                .ToList();

            return userAccounts;
        }

       

        public async Task<string> Edit(AccountViewModel model, string userName)
        {
            var user = repo.All<ApplicationUser>()
                .Where(x => x.UserName == userName)
                .FirstOrDefault();

            if (user == null)
            {
                throw new ArgumentNullException(MessageConstant.UserNullMessage);
            }

            var userId = user.Id;

            var oldAccount = repo.All<Account>()
                .Where(x => x.Id.ToString().ToUpper() == model.Id.ToUpper())
                .FirstOrDefault();

            if (oldAccount == null)
            {
                throw new ArgumentNullException(MessageConstant.AccountNullMessage);
            }

            oldAccount.AccountNumber = model.AccountNumber;
            oldAccount.Balance = model.Balance;
            oldAccount.NickName = model.NickName;
            
            await repo.SaveChangesAsync();

            return userId;
        }
    }
}
