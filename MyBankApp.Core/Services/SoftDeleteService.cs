﻿using MyBankApp.Core.Services.Contracts;
using MyBankApp.Infrastructure.Entities;
using MyBankApp.Infrastructure.Repositories;

namespace MyBankApp.Core.Services
{
    public class SoftDeleteService : ISoftDeleteService
    {
        private readonly IApplicatioDbRepository repo;

        public SoftDeleteService(IApplicatioDbRepository repo)
        {
            this.repo = repo;
        }

        public bool IsUserDeleted(string userEmail)
        {
            bool isDeleted = repo.All<ApplicationUser>()
                .Where(x => x.Email == userEmail)
                .Select(x => x.IsDeleted)
                .FirstOrDefault();

            return isDeleted;
        }
    }
}
