﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using MyBankApp.Core.Constants;
using MyBankApp.Core.Services.Contracts;
using MyBankApp.Core.ViewModels;
using MyBankApp.Infrastructure.Entities;
using MyBankApp.Infrastructure.Repositories;

namespace MyBankApp.Core.Services
{
    public class PurchaseService : IPurchaseService
    {
        private readonly IHttpContextAccessor context;
        private readonly IApplicatioDbRepository repo;

        public PurchaseService(IHttpContextAccessor context, IApplicatioDbRepository repo)
        {
            this.context = context;
            this.repo = repo;
        }

        public BuyCurrencyViewModel BuyCurrencyView(string name, decimal price)
        {
            var model = new BuyCurrencyViewModel
            {
                Name = name,
                Price = price,
            };

            return model;
        }

        public async Task<SellCurrencyViewModel> SellCurrencyView(string name, decimal amount, CurrencyViewModel currencyModel)
        {
            var value = currencyModel.Rates.First(r => r.Name == name).Percent;   

            var model = new SellCurrencyViewModel
            {
                Name = name,
                Amount = amount,
                Price = value
            };

            return model;
        }

        public async Task SellCurrency(SellCurrencyInputModel model, string userName)
        {
            try
            {
                var user = await repo.All<ApplicationUser>()
                    .Where(x => x.UserName == userName)
                    .Include(x => x.UserRates)
                    .ThenInclude(x => x.Rate)
                    .Include(x => x.Accounts)
                    .FirstOrDefaultAsync();

                if (user == null)
                {
                    throw new ArgumentNullException(MessageConstant.UserNullMessage);
                }

                var userCurrency = user.UserRates.Where(x => x.Rate.Name == model.Name).Select(x => x.Rate).FirstOrDefault();

                if (userCurrency == null)
                {
                    throw new ArgumentNullException(MessageConstant.CurrencyNullMessage);
                }

                var userAccount = user.Accounts.FirstOrDefault();

                if (userAccount == null)
                {
                    throw new ArgumentNullException(null, MessageConstant.AccountNullMessage);
                }


                userCurrency.Amount -= model.SellAmount;

                if (userCurrency.Amount < 0)
                {
                    throw new ArgumentNullException(null, MessageConstant.NotEnoughCurrencyMessage);
                }

                userAccount.Balance += model.AmountInUsd;

                if (userCurrency.Amount == 0)
                {
                    var userRate = user.UserRates.Where(x => x.Rate.Name == model.Name).FirstOrDefault();
                    userRate.Rate = null;
                    userRate.User = null;
                }

                await repo.SaveChangesAsync();
            }
            catch 
            {

                throw;
            }
            
        }

        public async Task BuyCurrency(BuyCurrencyInputModel model)
        {
            try
            {
                var userName = context.HttpContext.User.Identity.Name;

                var user = repo.All<ApplicationUser>()
                    .Include(x => x.Accounts)
                    .Include(x => x.UserRates)
                    .ThenInclude(x => x.Rate)
                    .FirstOrDefault(x => x.UserName == userName);

                var existingRate = repo.All<Rate>()
                    .Include(x => x.ApiCurrency)
                    .FirstOrDefault(x => x.Name == model.Name);

                var userAccount = user.Accounts.FirstOrDefault(x => x.Id.ToString().ToUpper() == model.UserAccountId);

                userAccount.Balance -= model.TotalPrice;

                var currUserRate = user.UserRates.FirstOrDefault(x => x.Rate.Name == model.Name);

                if (currUserRate != null)
                {
                    currUserRate.Rate.Amount += model.Amount;
                }
                else
                {

                    var rate = new Rate
                    {
                        Name = model.Name,
                        Percent = model.Price,
                        Amount = model.Amount,
                        ApiCurrency = existingRate.ApiCurrency
                    };

                    var userRate = new UserRate
                    {
                        Rate = rate,
                        User = user
                    };

                    await repo.AddAsync(userRate);
                }

                await repo.SaveChangesAsync();
            }
            catch (Exception e)
            {

                throw new Exception(e.Message);
            }
           
        }

        public async Task<ICollection<UserCurrenciesViewModel>> BoughtCurrencies(CurrencyViewModel currencyModel)
        {
            var userName = context.HttpContext.User.Identity.Name;

            var userCurrencies = await repo.All<ApplicationUser>()
                .Where(x => x.UserName == userName)
                .SelectMany(x => x.UserRates.Select(x => x.Rate))
                .ToListAsync();

            //var latestCurrencyValues = await initialPageService.GetCurrencyData();

            var model = userCurrencies.Select(x => new UserCurrenciesViewModel
            {
                Id = x.Id,
                Name = x.Name,
                Amount = x.Amount,
                CurrentPrice = currencyModel.Rates.First(r => r.Name == x.Name).Percent
            })
                .ToList();

            return model;
        }
    }
}
